

import 'package:flutter/material.dart';

class MakeCircle extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    paint.color = Colors.white12;
    var position = Offset(size.width /10, size.height / 10);
    canvas.drawCircle(position, 40.0, paint);
  }
  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}


// class MakeCurve extends CustomPainter {
//   @override
//   void paint(Canvas canvas, Size size) {
//     final paint = Paint();
   
//     var rect = Rect.fromLTWH(0, 0, size.width, size.height);
   
//     canvas.drawRect(rect, paint);
//     paint.color = Colors.yellow;
    
//     var path = Path();
//     path.lineTo(0, size.height);
//     path.lineTo(size.width, 0);

//     canvas.drawPath(path, paint);
//   }
//   @override
//   bool shouldRepaint(CustomPainter oldDelegate) => false;
// }
