

import 'package:agent_app/screens/my_info.dart';
import 'package:flutter/material.dart';



class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> 
 with TickerProviderStateMixin {
  int tabIndex=0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: tabIndex ==0 ?FirstScreen() 
        :tabIndex == 1? SecondScreen()
        : MyInfoScreen()
      ),

      bottomNavigationBar: BottomNavigationBar(
        elevation: 10,
        items:  <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home,//Image.asset("assets/icons/Ft1.png"), 
              color: Colors.grey,
               size: 30,
            ),
            activeIcon:Icon(Icons.home,//Image.asset("assets/icons/Ft1.png"),  
              color: Colors.blue,
               size: 30,
            ),
            title: Text('')
          ),
           BottomNavigationBarItem(
            icon:Icon(Icons.mail,// Image.asset("assets/icons/FT2.png"), 
              color: Colors.grey,
               size: 30,
             
            ),
            activeIcon:Icon(Icons.mail,// Image.asset("assets/icons/FT2.png"),  
              color: Colors.blue,
               size: 30,
            ),
            title: Text('')
          ),
           BottomNavigationBarItem(
            icon: Icon(Icons.person,//.asset("assets/icons/account.png",
            size: 30,
              color: Colors.grey,
            ),
            activeIcon: Icon(Icons.person,//.asset("assets/icons/account.png",
              color: Colors.blue,
               size: 30,
              ),
            title: Text('')
          )
        ],
        currentIndex: tabIndex,
        selectedItemColor: Colors.blueAccent,
        onTap: (int index){
          setState(() {
            tabIndex = index;
          });
        },
      ),
    );
  }

}

class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text("First Screen"),
      ),
    );
  }
}
class SecondScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text("Second Screen"),
      ),
    );
  }
}











