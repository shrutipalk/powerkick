import 'package:agent_app/helper/custom_paint_helper.dart';
import 'package:flutter/material.dart';



class MyInfoScreen extends StatefulWidget {
  @override
  _MyInfoScreenState createState() => _MyInfoScreenState();
}

class _MyInfoScreenState extends State<MyInfoScreen> {
 
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Container(
              height: deviceSize.height /2.4,
              color: Colors.white,
              child: Stack(
                children: <Widget>[
                  Container(
                    height: deviceSize.height /3.3,
                    decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.only(
                        bottomRight:  Radius.circular(30),
                        bottomLeft:  Radius.circular(30)
                      )
                    )
                  ),
                  cardWidget(deviceSize),
                  Positioned(
                    left: 10,
                    top: 40,
                    child: Container(
                      alignment: Alignment.center,
                      width: deviceSize.width,
                      child: Text("My Info",
                       style: TextStyle(
                         color:Colors.white,
                         fontSize: 20,
                         fontWeight: FontWeight.bold
                        ),
                      ),
                    )
                  ),
                  Positioned(
                    left: 10,
                    top: 30,
                    child: IconButton(
                      icon:Icon( Icons.arrow_back_ios,
                       color: Colors.white,
                      ),
                      onPressed: (){},
                    ),
                  ),

                  Positioned(
                    right:deviceSize.height/15,
                    top: deviceSize.height/15,
                    child: CustomPaint(
                      painter: MakeCircle(),
                      child: Container(
                       
                      ),
                    )
                  ),
                  Positioned(
                   left: deviceSize.height/15,
                    top: deviceSize.height/6,
                    child:  CustomPaint(
                      painter: MakeCircle(),
                      child: Container(
                       
                      ),
                    ),
                  )
                ],
              ), 
            ),
            listwidget()
          ],
        ),
      ) 
    );
  }




  Widget cardWidget(Size deviceSize){
    return  Positioned(
     bottom: 10,
      child: Container(
        alignment: Alignment.center,
         width: deviceSize.width,
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(40.0),
            ),
            elevation:10,
           child: Container(
             width: deviceSize.width/1.2,
             height: deviceSize.height / 3.8,
             child: Stack(
               children: <Widget>[
                 Center(
                   child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: deviceSize.height/10,//70
                          width: deviceSize.width/5,//70
                          // decoration: BoxDecoration(
                          //   color: Colors.blue,
                          //   borderRadius: BorderRadius.circular(80),
                          //   border: Border.all(width: 5.0,color: Colors.white)
                          // ),
                          child: Material(
                            borderRadius: BorderRadius.circular(80),
                            elevation: 10,
                            child: Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                color: Colors.blue,
                                borderRadius: BorderRadius.circular(80),
                                image: DecorationImage(
                                  image:AssetImage("assets/icons/use.png") ,
                                  fit: BoxFit.contain
                                )
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 10,),
                        Text("John Doe",
                          style: TextStyle(
                            fontSize: deviceSize.height/30,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        SizedBox(height: 2,),
                        Text("JohnDoe"),
                        SizedBox(height: 10,),
                        Text("+91 123 456 7890"),
                      ],
                    ),
                 ),
                 Positioned(
                   left: 0.0,
                   bottom: 0.0,
                   child: Container(
                     height: deviceSize.height/10,
                     width: deviceSize.height/10,
                     decoration: BoxDecoration(
                      //  color: Colors.red,
                       image: DecorationImage(
                         image:AssetImage("assets/icons/Effect.png") 
                         )
                     ),
                   )
                  ),
                   Positioned(
                   right: 0.0,
                   top: 0.0,
                   child:Container(
                     height: deviceSize.height/10,
                     width: deviceSize.height/10,
                     decoration: BoxDecoration(
                      //  color: Colors.red,
                       image: DecorationImage(
                         image:AssetImage("assets/icons/Effect_II.png") 
                         )
                     ),
                   )
                   
                  ),
               ],
             )
          ),
        ),
      ),
    );
  }

  Widget listwidget(){
    return  Expanded(
      child: SingleChildScrollView(
        padding: const EdgeInsets.only(left:20, right: 20),
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text("Setting"),
              leading: Image.asset("assets/icons/setting.png",
                
              ),
              onTap: (){},
            ),
            Divider(color:Colors.grey),
            ListTile(
              title: Text("History"),
              leading:  Image.asset("assets/icons/history.png",
               ),
              onTap: (){},
            ),
            Divider(color:Colors.grey),
            ListTile(
              title: Text("Support"),
              leading:  Image.asset("assets/icons/support.png",
             ),
              onTap: (){},
            ),
            Divider(color:Colors.grey),
            ListTile(
              title: Text("About"),
              leading:  Image.asset("assets/icons/about.png",
             ),
              onTap: (){},
            ),
            Divider(color:Colors.grey),
            ListTile(
              title: Text("Exit"),
              leading: Image.asset("assets/icons/exit.png",
              ),
              onTap: (){},
            ),
            
          ],
        )
      )
    );
  }
}
